" All system-wide defaults are set in $VIMRUNTIME/debian.vim (usually just
" /usr/share/vim/vimcurrent/debian.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vim/vimrc), since debian.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing debian.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
	" runtime! debian.vim



" Общие допoлнения:
	source /$HOME/.vim/options/vundle.vim
	source /$HOME/.vim/options/main.vim

" ru! %h/.vim/options/main.vim
" runtime! ./.vim/options/main.vim
" runtime /root/.vim/options/main.vim
" runtime! ~/.vim/options/main.vim
" runtime! /$HOME/.vim/options/main.vim


" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
	au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif



" Нумерация строк:
	set nu

" Без встоенных авто Отступов
	set noautoindent

	" set autoindent

	" set smartindent

	" set smarttab



" Отступы по формату документа
	if has("autocmd")
		filetype plugin indent on
	endif




" Кол-во символов пробелов, которые будут заменять \t:
	set tabstop=8
" Кол-во символов для авто-отсупов:
	set shiftwidth=8
	

" Кодировка - UTF-8:
	set encoding=utf-8
" Кодировка для терминальной стоки:
	set termencoding=utf-8
" Кодировка в которой будут сохраняться файлы:
	set fileencoding=utf-8

" Список предполагаемых кодировок, в порядке предпочтения:
	set fileencodings=utf8,koi8r,cp1251,cp866,ucs-2le

" Завершение строк в формате:
	set ffs=unix,dos,mac



" Управление мышью
	set mouse=a
	set mousemodel=popup


" Highlighing tcolor:
	" colorscheme zellner
	" colorscheme evening
	" colorscheme ron
	colorscheme darkblue

	set background=dark

" Переносить стрки выходящие за границы окна:
	set wrap

" Подсвечивать строку на которой курсор:
	" set cul


" Show (partial) command in status line:
	set showcmd



" Подсветку результатов поиска и совпадения скобок:
	set showmatch

" Поиск в процессе набора:
	set hlsearch

" Умная зависимость от регистра:
	set incsearch
	set smartcase

	set ignorecase
	
" Отображать скрытый текст:
	set conceallevel=1

" Спецсимволы (TAB | Пробелы в конце строки | nbsp | EOL | Символ переноса строки | Символ в начале переносённой строки | Скрытый текст):
	set listchars=
		\tab:➧·,
		\trail:•,
		\nbsp:∵,
		\eol:¶,
		\extends:➬,
		\precedes:.
		" не работает \conseal:.

" Устанвовить значения:
	set list


" Не выводить статусную строку, используется vim-airline:
	set laststatus=0
	" set statusline=%f-%t\ %y%m%r[%{&fileencoding}]%<[%{strftime(\"%d.%m.%y\",getftime(expand(\"%:p\")))}]%k%=%-14.(%l,%c%V%)\ %P



" Допустимое количесто строк от курсора до верхнего и нижнего края:
	set scrolloff=8



" Русский текст:
	set keymap=russian-jcukenwin


" Искать на кириллице: 
	set iminsert=0
	set imsearch=0


" Регистро-независимый поиск:
	set ignorecase

" Подсветить все найденные совпадения:
	set hlsearch


" Возможность скрытия частей кода:
	set foldmethod=syntax
	set foldcolumn=3



