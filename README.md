# README #

Скопировать данный репозиторий:

```
#!git

git clone git@bitbucket.org:T-Soloveychik/unix-vim.git [dir_name]
```

В директории репозитория активировать **git sumodule**


```
#!git

git submodule update --init --recursive
```

VIM будет проверять на наличие в домашней директории файла настроек **$HOME/.vimrc**, по этому нужно создать жёстку ссылку на файл в скопированном репозитории:

```
#!shell

ln [dir_name]/vimrc.vim $HOME/.vimrc
```

Активировать все дополнения из консоли:

```
#!#!/bin/bash

vim +PluginInstall +qall
```

... или запущенном экземпляре vim:

```
#!VimL

:PluginInstall
```