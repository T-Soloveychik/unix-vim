" All system-wide defaults are set in $VIMRUNTIME/debian.vim (usually just
" /usr/share/vim/vimcurrent/debian.vim) and sourced by the call to :runtime
" you can find below.  If you wish to change any of those settings, you should
" do it in this file (/etc/vim/vimrc), since debian.vim will be overwritten
" everytime an upgrade of the vim packages is performed.  It is recommended to
" make changes after sourcing debian.vim since it alters the value of the
" 'compatible' option.

" This line should not be removed as it ensures that various options are
" properly set to work with the Vim-related packages available in Debian.
runtime! debian.vim


" Uncomment the following to have Vim jump to the last position when
" reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif


" Source a global configuration file if available
if filereadable("/etc/vim/vimrc.local")
  source /etc/vim/vimrc.local
endif


" Numereation :
	set nu

" Без встоенных авто Отступов
	set noautoindent

	" set autoindent

	" set smartindent

	" set smarttab



" Отступы по формату документа
	if has("autocmd")
	  filetype plugin indent on
	endif



" список предполагаемых кодировок, в порядке предпочтения
	set encoding=utf-8
	" set charset translation encoding
	set termencoding=utf-8
	" set terminal encoding
	set fileencoding=utf-8
	" set save encoding
	set fileencodings=utf8,koi8r,cp1251,cp866,ucs-2le

" Управление мышью
	set mouse=a
	set mousemodel=popup

	syntax on

" Highlighing tcolor:
	" colorscheme zellner
	" colorscheme evening
	" colorscheme ron
	colorscheme darkblue


	set background=dark


	set wrap

" Подсвечивать строку на которой курсор:
	set cul


" Show (partial) command in status line.
	set showcmd


" Кол-во символов пробелов, которые будут заменять \t
	set tabstop=8
	set shiftwidth=8


" Подсветку результатов поиска и совпадения скобок:
	set showmatch

" Поиск в процессе набора
	set hlsearch

" умная зависимость от регистра. Детали `:h smartcase`
	set incsearch
	set smartcase

	set ignorecase

" Перехват мышки - отключить/включить:
	noremap <F2> :call ToggleMouse()<cr>
	function! ToggleMouse()
		if &mouse == 'a'
			set mouse=
			set nonumber
			set nolist
			set foldcolumn=0
                        set paste
			echo "Mouse usage disabled"
		else
			set mouse=a
			set number
			set list
			set foldcolumn=3
                        set nopaste
			echo "Mouse usage enabled"
		endif
	endfunction


"Edit vim configs:
	map <F3> :tabnew /etc/vim/vimrc<cr>
	vmap <F3> <esc>:tabnew /etc/vim/vimrc<cr>
	imap <F3> <esc>:tabnew /etc/vim/vimrc<cr>

	map <F5> :e<cr>
	vmap <F5> <esc>:e<cr>
	imap <F5> <esc>:e<cr>

	map <F7> :w<cr>
	vmap <F7> <esc>:w<cr>
	imap <F7> <esc>:w<cr>

	map <F8> :wq<cr>
	vmap <F8> <esc>:wq<cr>
	imap <F8> <esc>:wq<cr>

	map <F10> :q!<cr>
	vmap <F10> <esc>:q!<cr>
	imap <F10> <esc>:q!<cr>


" Показать все менюшки:
	map <F4> :emenu <tab>
	vmap <F4> <esc>:emenu <tab>
	imap <F4> <esc>:emenu <tab>



" Нужно для того, чтобы работало меню:
	set wcm=<Tab>
	set wildmenu
	"set wildmode

" Меню:
	menu Menu.F2\ -\ Отключить/Включить\ всё  <esc><F2>
	menu Menu.F3\ -\ Открыть\ настройки\ vim  <esc><F3>
	menu Menu.F5\ -\ Перезагрузить\ файл <esc><F5>
	menu Menu.F6\ -\ Вкл/Вкл\ проврку\ языка <esc><F6>
	menu Menu.F7\ -\ Сохранит\ файл <esc><F7>
	menu Menu.F8\ -\ Сохранит\ файл\ и\ выйти <esc><F8>
	menu Menu.F10\ -\ Выйти\ без\ сохранения! <esc><F10>
	map <F9> <esc>:emenu Menu.<Tab>



" Меню проверки орфографии:
	menu SetSpell.вариант\ для\ замены z=
	menu SetSpell.off :set nospell<CR>
	menu SetSpell.ru  :set spl=ru spell<CR>
	menu SetSpell.en  :set spl=en spell<CR>
	map <F6> :emenu SetSpell.<Tab>



" TABs ENDs TRAILs :
	set listchars=tab:<\ ,trail:·,nbsp:.,eol:¶,extends:>,precedes:.

	set list

	set ffs=unix,dos,mac
	set fencs=utf-8,cp1251,koi8-r,ucs-2,cp86

	"set guioptions=a " Otions
	"set guioptions=a " MENU



" always show the status line:
	set laststatus=2
	set statusline=%f-%t\ %y%m%r[%{&fileencoding}]%<[%{strftime(\"%d.%m.%y\",getftime(expand(\"%:p\")))}]%k%=%-14.(%l,%c%V%)\ %P



" Lines on TOP:
	set scrolloff=8


" leyers:
	set foldmethod=syntax
	set foldcolumn=3



" Русский текст:
	set keymap=russian-jcukenwin


" Не помню
	set iminsert=0


" Искать на английском:
	set imsearch=0
" Не помню что:
	set ic
" Не помню что:
	set hls


" Авто-дополнение написанного по уже писанному:
	function! InsertTabWrapper(direction)
		let col = col('.') - 1
		if !col || getline('.')[col - 1] !~ '\k'
			return "\<tab>"
		elseif "backward" == a:direction
			return "\<c-p>"
		else
			return "\<c-n>"
		endif
	endfunction
	inoremap <tab> <c-r>=InsertTabWrapper ("forward")<cr>
	inoremap <s-tab> <c-r>=InsertTabWrapper ("backward")<cr>


" Перенос строк Ctrl + Shift + (вверх/вниз)
" Перенос строк F11 - вниз / F12 -  вверх
	nnoremap <c-up> :m .+1<CR>
	nnoremap <F11> :m .+1<CR>
	nnoremap <F12> :m .-2<CR>==
	inoremap <F11> <Esc>:m .+1<CR>==gi
	inoremap <F12> <Esc>:m .-2<CR>==gi
	vnoremap <F11> :m '>+1<CR>gv=gv
	vnoremap <F12> :m '<-2<CR>gv=gv
