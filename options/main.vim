


" Перехват мышки - отключить/включить:
	noremap <F2> :call ToggleMouse()<cr>
	function! ToggleMouse()
		if &mouse == 'a'
			set mouse=
			set nonumber
			set nolist
			set foldcolumn=0
                        set paste
			echo "Mouse usage disabled"
		else
			set mouse=a
			set number
			set list
			set foldcolumn=3
                        set nopaste
			echo "Mouse usage enabled"
		endif
	endfunction


"Edit vim configs:
	map <F3> :tabnew /etc/vim/vimrc<cr>
	vmap <F3> <esc>:tabnew /etc/vim/vimrc<cr>
	imap <F3> <esc>:tabnew /etc/vim/vimrc<cr>

	map <F5> :e<cr>
	vmap <F5> <esc>:e<cr>
	imap <F5> <esc>:e<cr>

	map <F7> :w<cr>
	vmap <F7> <esc>:w<cr>
	imap <F7> <esc>:w<cr>

	map <F8> :wq<cr>
	vmap <F8> <esc>:wq<cr>
	imap <F8> <esc>:wq<cr>

	map <F10> :q!<cr>
	vmap <F10> <esc>:q!<cr>
	imap <F10> <esc>:q!<cr>


" Показать все менюшки:
	map <F4> :emenu <tab>
	vmap <F4> <esc>:emenu <tab>
	imap <F4> <esc>:emenu <tab>



" Нужно для того, чтобы работало меню:
	set wcm=<Tab>
	set wildmenu
	"set wildmode

" Меню:
	menu Menu.F2\ -\ Отключить/Включить\ всё  <esc><F2>
	menu Menu.F3\ -\ Открыть\ настройки\ vim  <esc><F3>
	menu Menu.F5\ -\ Перезагрузить\ файл <esc><F5>
	menu Menu.F6\ -\ Вкл/Вкл\ проврку\ языка <esc><F6>
	menu Menu.F7\ -\ Сохранит\ файл <esc><F7>
	menu Menu.F8\ -\ Сохранит\ файл\ и\ выйти <esc><F8>
	menu Menu.F10\ -\ Выйти\ без\ сохранения! <esc><F10>
	map <F9> <esc>:emenu Menu.<Tab>



" Меню проверки орфографии:
	menu SetSpell.вариант\ для\ замены z=
	menu SetSpell.off :set nospell<CR>
	menu SetSpell.ru  :set spl=ru spell<CR>
	menu SetSpell.en  :set spl=en spell<CR>
	map <F6> :emenu SetSpell.<Tab>





" Авто-дополнение написанного по уже писанному:
	function! InsertTabWrapper(direction)
		let col = col('.') - 1
		if !col || getline('.')[col - 1] !~ '\k'
			return "\<tab>"
		elseif "backward" == a:direction
			return "\<c-p>"
		else
			return "\<c-n>"
		endif
	endfunction
	inoremap <tab> <c-r>=InsertTabWrapper ("forward")<cr>
	inoremap <s-tab> <c-r>=InsertTabWrapper ("backward")<cr>


" Перенос строк Ctrl + Shift + (вверх/вниз)
" Перенос строк F11 - вниз / F12 -  вверх
	nnoremap <c-up> :m .+1<CR>
	nnoremap <F11> :m .+1<CR>
	nnoremap <F12> :m .-2<CR>==
	inoremap <F11> <Esc>:m .+1<CR>==gi
	inoremap <F12> <Esc>:m .-2<CR>==gi
	vnoremap <F11> :m '>+1<CR>gv=gv
	vnoremap <F12> :m '<-2<CR>gv=gv
