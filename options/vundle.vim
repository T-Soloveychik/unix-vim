set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

Plugin 'bling/vim-airline'
Plugin 'tomtom/tcomment_vim'
Plugin 'scrooloose/nerdtree'
" Plugin 'chriskempson/base16-vim'
" Plugin 'matze/vim-move'
" Plugin 'bling/vim-bufferline'
Plugin 'terryma/vim-multiple-cursors'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required




" Plugin: Airline
let g:airline_powerline_fonts = 0
let g:airline_left_sep = ''
let g:airline_right_sep = ''
let g:airline_symbols = {
        \ 'linenr'     : '¶',
        \ 'branch'     : '☢',
        \ 'paste'      : 'ρ',
        \ 'readonly'   : '✦',
        \ 'whitespace' : 'Ξ',
        \ }
let g:airline_mode_map = {
        \ '__' : '-',
        \ 'n'  : 'N',
        \ 'i'  : 'I',
        \ 'R'  : 'R',
        \ 'c'  : 'C',
        \ 'v'  : 'V',
        \ 'V'  : 'V',
        \ '^V' : 'VB',
        \ 's'  : 'S',
        \ 'S'  : 'S',
        \ '^S' : 'S',
        \ }

let g:airline_theme = 'tomorrow'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'
let g:airline#extensions#tabline#buffer_min_count = 2
let g:airline_paste_symbol = ''

